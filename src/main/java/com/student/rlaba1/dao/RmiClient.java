package com.student.rlaba1.dao;

import com.student.rlaba1.entity1.Vouchers;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

public class RmiClient implements LayerDAO {
    LayerDAO obj = null;

    @Override
    public List<Vouchers> parseXml(){
        try{
            obj = (LayerDAO)Naming.lookup("//localhost:1099/RmiServer");
            return obj.parseXml();
        } catch(MalformedURLException | NotBoundException | RemoteException ex){
            ex.printStackTrace();
            return null;
        }
    }
    
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Press to start");
        if(br.readLine() != null){
            RmiClient cli = new RmiClient();
            System.out.println(cli.parseXml());
        }
    }
}
