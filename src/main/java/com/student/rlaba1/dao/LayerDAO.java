package com.student.rlaba1.dao;

import com.student.rlaba1.entity1.Vouchers;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface LayerDAO extends Remote {
    
    List<Vouchers> parseXml() throws RemoteException;
}
