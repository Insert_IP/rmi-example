package com.student.rlaba1.dao;

import com.student.rlaba1.builder.VoucherDOMBuilder;
import com.student.rlaba1.entity1.Vouchers;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

public class ClientDAO extends UnicastRemoteObject implements LayerDAO {

    public ClientDAO() throws RemoteException {
    }

    public ClientDAO(int port) throws RemoteException {
        super(port);
    }

    public ClientDAO(int port, RMIClientSocketFactory csf, RMIServerSocketFactory ssf) throws RemoteException {
        super(port, csf, ssf);
    }
    
    @Override
    public List<Vouchers> parseXml(){
        VoucherDOMBuilder builderDOM = new VoucherDOMBuilder();
        builderDOM.buildSetVouchers("touristVouchers.xml");
        return builderDOM.getVouchers();
    }
    
    public static void main(String[] args) {
        System.out.println("Server started");
        
        
        try{
            LocateRegistry.createRegistry(1099);
            System.out.println("RMI registry created");
        } catch(RemoteException ex){
            ex.printStackTrace();
        }
        
        try{
            ClientDAO obj = new ClientDAO();
            Naming.rebind("//localhost/RmiServer", obj);
        } catch(RemoteException | MalformedURLException ex){
            ex.printStackTrace();
        }
    }
    
}
