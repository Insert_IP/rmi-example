package com.student.rlaba1.builder;

import com.student.rlaba1.entity1.Entertainment;
import com.student.rlaba1.entity1.Sanatorium;
import com.student.rlaba1.entity1.Vouchers;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class VoucherDOMBuilder {

    private final Logger LOGGER = Logger.getLogger(VoucherDOMBuilder.class);
    private ArrayList<Vouchers> vouchers;
    private DocumentBuilder docBuilder;

    public VoucherDOMBuilder() {
        this.vouchers = new ArrayList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            LOGGER.error("Configuration error occured");
        }
    }

    public ArrayList<Vouchers> getVouchers() {
        return vouchers;
    }

    public void buildSetVouchers(String fileName) {
        Document document = null;
        try {
            document = docBuilder.parse(fileName);
            Element root = document.getDocumentElement();
            NodeList list = root.getElementsByTagName("sanatorium");
            for (int i = 0; i < list.getLength(); i++) {
                Element element = (Element) list.item(i);
                Sanatorium sanatorium = buildSanatorium(element);
                vouchers.add(sanatorium);
                System.out.println(vouchers.size());
            }
            list = root.getElementsByTagName("entertaining");
            for (int i = 0; i < list.getLength(); i++) {
                Element element = (Element) list.item(i);
                Entertainment entertainment = buildEntertainment(element);
                vouchers.add(entertainment);
                System.out.println(vouchers.size());
            }
        } catch (SAXException ex) {
            LOGGER.error("Parse error");
        } catch (IOException ex) {
            LOGGER.fatal("File loading error");
            throw new RuntimeException();
        }
    }

    private Sanatorium buildSanatorium(Element elem) {
        Sanatorium obj = new Sanatorium();
        obj.setId(Integer.parseInt(getElementTextContent(elem, "id").substring(1)));
        obj.setCountry(getElementTextContent(elem, "country"));
        obj.setDays(Integer.parseInt(getElementTextContent(elem, "days")));
        obj.setCost(Integer.parseInt(getElementTextContent(elem, "cost")));
        obj.setTreatment(elem.getAttribute("treatment"));
        obj.setLocation(elem.getAttribute("location"));
        return obj;
    }
    
    private Entertainment buildEntertainment(Element elem) {
        Entertainment obj = new Entertainment();
        obj.setId(Integer.parseInt(getElementTextContent(elem, "id").substring(1)));
        obj.setCountry(getElementTextContent(elem, "country"));
        obj.setDays(Integer.parseInt(getElementTextContent(elem, "days")));
        obj.setCost(Integer.parseInt(getElementTextContent(elem, "cost")));
        obj.getHotel().setStars(Integer.parseInt(getElementTextContent(elem, "stars")));
        obj.getHotel().setRooms(Integer.parseInt(getElementTextContent(elem, "rooms")));
        obj.getHotel().setConditioner(Boolean.parseBoolean(getElementTextContent(elem, "conditioner")));
        return obj;
    }

    private static String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        String text = node.getTextContent();
        return text;
    }
}
