package com.student.rlaba1.entity1;

import java.io.Serializable;
import java.util.Objects;

public class Sanatorium extends Vouchers implements Serializable {
    private String treatment;
    private String location;

    public Sanatorium() {
    }

    public Sanatorium(String treatment, String location, int id, String country, int days, int cost) {
        super(id, country, days, cost);
        this.treatment = treatment;
        this.location = location;
    }

    public String getTreatment() {
        return treatment;
    }

    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Sanatorium: {\nID: " + this.getId() + "\nCountry: " + this.getCountry() + "\nDays: " + this.getDays() + "\nCost: " + this.getCost() + "\nTreatment: " + treatment + "\nLocation: " + location + "\n}\n\n";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.treatment);
        hash = 67 * hash + Objects.hashCode(this.location);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if(this == obj){
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sanatorium other = (Sanatorium) obj;
        if (!Objects.equals(this.treatment, other.treatment)) {
            return false;
        }
        if (!Objects.equals(this.location, other.location)) {
            return false;
        }
        return true;
    }
}
