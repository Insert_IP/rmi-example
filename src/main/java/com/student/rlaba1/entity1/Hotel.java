package com.student.rlaba1.entity1;

import java.io.Serializable;

public class Hotel implements Serializable {
    private int stars;
    private int rooms;
    private boolean conditioner;

    public Hotel() {
    }

    public Hotel(int stars, int rooms, boolean conditioner) {
        this.stars = stars;
        this.rooms = rooms;
        this.conditioner = conditioner;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public boolean isConditioner() {
        return conditioner;
    }

    public void setConditioner(boolean conditioner) {
        this.conditioner = conditioner;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + this.stars;
        hash = 53 * hash + this.rooms;
        hash = 53 * hash + (this.conditioner ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Hotel other = (Hotel) obj;
        if (this.stars != other.stars) {
            return false;
        }
        if (this.rooms != other.rooms) {
            return false;
        }
        if (this.conditioner != other.conditioner) {
            return false;
        }
        return true;
    }
}
