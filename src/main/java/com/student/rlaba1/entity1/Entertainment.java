package com.student.rlaba1.entity1;

import java.io.Serializable;
import java.util.Objects;

public class Entertainment extends Vouchers implements Serializable{
    private Hotel hotel = new Hotel();

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }
    
    @Override
    public String toString() {
        return "Entertainment: {\nID: " + this.getId() + "\nCountry: " + this.getCountry() + "\nDays: " + this.getDays() + "\nCost: " + this.getCost() + "\nStars: " + hotel.getStars() + "\nRooms: " + hotel.getRooms() + "\nConditioner: " + hotel.isConditioner() + "\n}\n\n";
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.hotel);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if(this == obj) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Entertainment other = (Entertainment) obj;
        if (!Objects.equals(this.hotel, other.hotel)) {
            return false;
        }
        return true;
    }
}
