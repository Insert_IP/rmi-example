package com.student.rlaba1.entity1;

import java.io.Serializable;

public abstract class Vouchers implements Serializable {
    private int id;
    private String country;
    private int days;
    private int cost;

    public Vouchers() {
    }

    public Vouchers(int id, String country, int days, int cost) {
        this.id = id;
        this.country = country;
        this.days = days;
        this.cost = cost;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
