package com.student.rlaba1.entity1;

public enum VoucherEnum{
    TOURIST_VOUCHERS("tourist-vouchers"),
    SANATORIUM("sanatorium"),
    ENTERTAINING("entertaining"),
    TREATMENT("treatment"),
    LOCATION("location"),
    ID("id"),
    COUNTRY("days"),
    DAYS("days"),
    COST("cost"),
    STARS("stars"),
    ROOMS("rooms"),
    CONDITIONER("conditioner"),
    HOTEL("hotel");
    
    private String value;

    private VoucherEnum(String value) {
        this.value = value;
    }
    
    public String getValue(){
        return this.value;
    }
}
